#include <bits/stdc++.h>

// graf skierowany, acykliczny (niekoniecznie spójny)
// dlaczego nie las? - bo mogą być dwa projekty wskazujące na ten sam
// (trzeba wykonac najpierw 1 zanim sie zrobi 2 i 3)

// dynamiczne najkrotsze sciezki tez na takich grafach
// sortowanie topologiczne tez na takich grafach sie robi
// czyli układanie na osi x wierzchołków tak elegancko że ładnie idą krawędzie
// ale to nie to

// jak uprościć??
// gdyby p_i = 1 : w czasie stałym - po prostu liczba projektow = liczbie programistow
// gdyby wszystkie projekty miałyby być zrealizowane : rozw = max(p_1, .., p_n)

// UWAGA robimy projekty po kolei, zeby oszczedzic na liczbie programistow
// czyli nie robimy jednoczesnie nic, to bez sensu, czas na wykonanie jest infty

// żeby dało się wykonać jakiś zależny projekt to zawsze ofc trzeba zacząć od końcówek
// czyli zrobić jakiś graf w drugą stronę?
// na razie ze wszystkimi wierzchołkami tej samej wagi:
// trzymac w wierzchołku zmienną od_ilu_zalezy? (outdeg(v))
// lub ile od niego zalezy? (indeg(v))
// mozna to trzymac w tablicy vectorow o ind = nr wierzcholkow

// DLA WIERZCHOLKOW O TYCH SAMYCH WAGACH:
// R = {} <- rozwiazanie
// Q = kolejka wierzcholkow o indeg() = 0 (od nikogo nie zaleza)
/*
while |R| < k do
    u <- Q.pop()
    dodaj u na koniec R
    for v \in indeg(u) dp
        outdeg(v)--
        // ^skasowana krawedz wychodzaca ostatnia
        // outdeg() == 0 <- mozna myslec o wykonaniu tego proj
        if (outdeg(v) == 0)
            Q.insert(v)
*/
// NAPISALIŚMY SORTOWANIE TOPOLOGICZNE WŁAŚNIE^ (tylko bez while |R|<k)
// PORZADEK TOPOLOGICZNY:
// uporzadkowanie wierzcholkow na osi tak że "wszystkie krawędzie skierowane w lewo"

// DLA WIERZCHOLKOW O RÓŻNYCH WAGACH: (co zmienić?)
// może się zablokować - nie będzie wierzchołka do zdjęcia? możemy utknąć na pocz. kolejki
// kolejka priorytetowa typu min? zachłan!
// czy działa?
// napisany:
// R = {} <- rozwiazanie
// Q = kolejka priotytetowa wierzcholkow o indeg() = 0 (od nikogo nie zaleza)
// outdeg trzyma pare nr wierzchołka i priorytet
/*
while |R| < k do
    (u, p_u) <- Q.ExtractMin()
    dodaj (u, p_u) na koniec R
    for v \in indeg(u) dp
        outdeg(v)--
        // ^skasowana krawedz wychodzaca ostatnia
        // outdeg() == 0 <- mozna myslec o wykonaniu tego proj
        if (outdeg(v) == 0)
            Q.insert(v)
return max priority (R)
*/

using namespace std;
const long long MAX_P = 100005;

priority_queue<pair<long long, long long>> qmin;
int liczba_proj, m, min_liczba_projektow;
long long res = -1;
long long waga[MAX_P] = {0};
long long od_ilu_zaleze[MAX_P] = {0};
vector<long long> co_zalezy_ode_mnie[MAX_P];

void get_input() {
    cin >> liczba_proj >> m >> min_liczba_projektow;
    int a;
    for (int i = 1; i <= liczba_proj; i++) {
        cin >> a;
        waga[i] = -a;
    }
    for (int i = 0; i < m; i++) {
        long long a, b; // a zależy od b, czyli najpierw b
        cin >> a >> b;
        co_zalezy_ode_mnie[b].push_back(a);
        od_ilu_zaleze[a]++;
    }
}

void extract_one() {
    pair<long long, long long> p = qmin.top();
    qmin.pop();
    //cout<<p.first<<" "<<p.second<<endl;
    res = max(res, -p.first);
    long long w = p.second;
    for (long long i = 0; i < co_zalezy_ode_mnie[w].size(); i++) {
        long long zalezny = co_zalezy_ode_mnie[w][i];
        od_ilu_zaleze[zalezny]--;
        if (od_ilu_zaleze[zalezny] == 0) {
            qmin.push(make_pair(waga[zalezny], zalezny));
        }
    }
    min_liczba_projektow--;
}

int main() {
    ios_base::sync_with_stdio(false);
    get_input();
    
    for (int i = 1; i <= liczba_proj; i++) {
        if (od_ilu_zaleze[i] == 0) {
            qmin.push(make_pair(waga[i], i));
        }
    }

    while (min_liczba_projektow > 0) {
        extract_one();
    }
    cout << res << endl;
}