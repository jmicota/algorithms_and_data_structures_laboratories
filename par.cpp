#include <bits/stdc++.h>

using namespace std;

// dla kazdego wierzcholka chcemy: jego wysokosc (ile ma dzieci w dol maks? i z ktorej strony?)
// czy istnieje dla v i d wierzcholek u oddalony od v o d <=> czy d <= max(wysokosc(lsyn), wys(psyn), wys(dodsyn,
// stw przez wziecie v jako root)

// dla kazdego v -> maks odl, najdalszy wierzcholek

// najdalszy(v) - taki wierzcholek u ze maksymalizuje dist(u,v)
// max_odl(v) = dist(v, najdalszy(v))

// PLAN
// 1. policzyc funkcje max_odl(v), najdalszy(v) w O(n) dla wszystkich v
// 2. efektywna nawigacja w drzewie
//      jump(v, u, d) = z    t.że z \in Path(v,u) && dist(v,z)=d
//      jumpUp(v, d) = z     t.że z \in Path(v,root) && dist(v,z)=d
//      LCA(v, u) = q        najniższy wspólny przodek v i u

// jesli max_odl < d -> -1
// else nasz szukany na penwo na drodze do najdalszy -> idzmy wiec jumpem i zwrocmy z
// nie stac nas na ukorzenianie w roznych wierzcholkach wiec powiedzmy ze istnieje umowny staly root
// wtedy ma sens uzywanie LCA i jumpUp

/** IMPLEMENTACJA
1. najdalszy(V)
        najglebszy(v) - najdalszy sposrod potomkow
        najpoza(v) - najdalszy poza poddrzewem v - potomek rodzenstwa lub najpoza(rodzic(v))
    for v \in T policz najglebszy(v);
    teraz licz najpoza(v);
    najpoza(v) = max(najglebszy(brat(v)+2, najpoza(rodzic(v))+1);
    
    należy to tak napisac zeby nie isc dwa razy po tej samej krawedzi!
    rekurencyjnie od korzenia w dol, i pamietajmy że brat czy dziecko może nie istniec!

    najdalszy(v) = max(najglebszy(v), najpoza(v))

2. redukcja do elementu na ścieżce
        jump(v, u, d)
    wołamy to wiedząc już że jest dokąd skakać (w kierunku najdalszego, czyli z = jump(v, najdalszy(v), d))

    Jak odpowiadać na pytania o dist(v,u)?
    dist(v,u) = głebokoscOdKorzenia(v) + gł(u) - 2*gł(LCA(v,u))
    Tylko jak znaleźć LCA?
    LCA(v,u) = ..

    Jak z jumpUp i dist(v,u) zrobić jump?
    - jeśli v pod u to jumpUP(v, u, d)
    - jeśli u pod v to jumpUp(u, v, dist(v, u) - d)
    - jeśli są w "obok"
        if d < dist(v, LCA(u,v)) return jumpUp(v,d)
        else return jumpUp(u, dist(v,u) - d)

3. jumpUp? LCA?
        wyszukiwanie k-przodka! :
        przodek[][] to tablica wykładniczych skoków
        każdy wierzchołek trzyma swoich wykładniczych przodków
        przodek[v][k] trzyma 2^k przodka v
        przodek[v][k+1] = przodek[przodek[v][k]][k]
        ^ to skacze o potęgi dwojki, a jumpUp chce o dowolną liczbe
        + co jak wyskoczymy poza drzewo??

        jumpUp z k-przodka:
        rozbijamy d na binarny i bedziemy uzywac k-przodka
        jumpUp(v,d)    d = 2^i1 + 2^i2 + ..
        u := v
        for i in i1, i2, ..

        LCA(v,u)
        wyrownujemy glebokosc z k-przodka
        wyszukujemy binarnie: 
            jeśli przodek[v][k] != przodek[u][k] to: 
                v = przodek[v][k]
                u = przodek[u][k]
            k--
            (LCA(u,v)jest maksymalnie 2^{k+1} przodkiem v i u)
**/

int n, a, b;

int main() {
    cin >> n;

    for (int i = 0; i < n; i++) {
        cin >> a >> b;
    }
}