#include <bits/stdc++.h>

// JUSTYNA MICOTA 418427

using namespace std;
using vtype = long long;
using values_t = pair<vtype, vtype>;
const int MAX = 1000005;
const int NO_VALUE = -1;
int field_height, field_width, nr_of_queries;

struct interval_node {
    vtype minimum; // minimum 
    vtype maximum;
    int left_end;
    int right_end;
    int nr_of_children;
    struct interval_node *left;
    struct interval_node *right;
};
using interval_node = struct interval_node;
interval_node* interval_rows[MAX];

struct two_dim_interval_node {
    int min_row;
    int max_row;
    interval_node* merged_tree;
    struct two_dim_interval_node* left;
    struct two_dim_interval_node* right;
};
using two_dim_interval_node = struct two_dim_interval_node;
two_dim_interval_node* ultimate_interval_tree;

/* Methods responsible for building 2d interval structure during preprocessing. 
   Performed only once. */
/* Complexity of building = O(n * m) */
namespace preprocessing {

interval_node* new_interval_leaf(int left_end_arg, int right_end_arg, 
                                 vtype minimum_arg, vtype maximum_arg) {
    interval_node *node = new interval_node;
    node->left_end = left_end_arg;
    node->right_end = right_end_arg;
    node->minimum = minimum_arg;
    node->maximum = maximum_arg;
    node->left = NULL;
    node->right = NULL;
    node->nr_of_children = 0;
    return node;
}

// merged trees should have identical structures
interval_node* merge_trees(interval_node* node1, interval_node* node2) {
    if (node1 != NULL && node2 != NULL) {
        interval_node* merged = new interval_node;
        merged->minimum = min(node1->minimum, node2->minimum);
        merged->maximum = max(node1->maximum, node2->maximum);
        assert(node1->left_end == node2->left_end && 
               node1->right_end == node2->right_end && 
               node1->nr_of_children == node2->nr_of_children);
        merged->left_end = node1->left_end;
        merged->right_end = node1->right_end;
        merged->nr_of_children = node1->nr_of_children;
        merged->left = merge_trees(node1->left, node2->left);
        merged->right = merge_trees(node1->right, node2->right);
        return merged;
    }
    return NULL;
}

interval_node* connect_by_parent(interval_node* left_node, interval_node* right_node) {
    interval_node* parent_node = new interval_node;
    parent_node->left = left_node;
    parent_node->right = right_node;
    if (right_node == NULL) {
        parent_node->minimum = left_node->minimum;
        parent_node->maximum = left_node ->maximum;
        parent_node->left_end = left_node->left_end;
        parent_node->right_end = left_node->right_end;
        parent_node->nr_of_children = left_node->nr_of_children + 1;
        return parent_node;
    }
    else if (right_node != NULL && left_node != NULL) {
        parent_node->minimum = min(left_node->minimum, right_node->minimum);
        parent_node->maximum = max(left_node->maximum, right_node->maximum);
        parent_node->left_end = left_node->left_end;
        parent_node->right_end = right_node->right_end;
        parent_node->nr_of_children = left_node->nr_of_children + 
                                      right_node->nr_of_children + 2;
        return parent_node;
    }
    return NULL;
}

two_dim_interval_node* two_dim_connect_by_parent(two_dim_interval_node* left_node, 
                                                 two_dim_interval_node* right_node) {
    two_dim_interval_node* parent = new two_dim_interval_node;
    parent->left = left_node;
    parent->right = right_node;
    if (right_node == NULL) {
        parent->min_row = left_node->min_row;
        parent->max_row = left_node->max_row;
        parent->merged_tree = left_node->merged_tree;
        return parent;
    }
    else if (right_node != NULL && left_node != NULL) {
        parent->min_row = min(left_node->min_row, right_node->min_row);
        parent->max_row = max(left_node->max_row, right_node->max_row);
        parent->merged_tree = merge_trees(left_node->merged_tree, right_node->merged_tree);
        return parent;
    }
    return NULL;
}

two_dim_interval_node* pack_tree_into_two_dim_node(interval_node* node, int row_nr) {
    two_dim_interval_node* two_dim_node = new two_dim_interval_node;
    two_dim_node->left = NULL;
    two_dim_node->right = NULL;
    two_dim_node->merged_tree = node;
    two_dim_node->max_row = row_nr;
    two_dim_node->min_row = row_nr;
    return two_dim_node;
}

void rec_build_interval_tree(interval_node* nodes[], int len, 
                             interval_node* nodes_temp[], int row_nr) {
    if (len > 1) {
        for (int i = 0; i < len; i += 2) {
            if (i + 1 == len)
                nodes_temp[i / 2] = connect_by_parent(nodes[i], NULL);
            else
                nodes_temp[i / 2] = connect_by_parent(nodes[i], nodes[i + 1]);
        }
        nodes = nodes_temp;
        rec_build_interval_tree(nodes, (len + 1) / 2, nodes_temp, row_nr);
    }
    else interval_rows[row_nr] = nodes[0];
}

void rec_build_two_dim_tree(two_dim_interval_node* nodes[], int len,
                            two_dim_interval_node* nodes_temp[]) {
    if (len > 1) {
        for (int i = 0; i < len; i += 2) {
            if (i + 1 == len)
                nodes_temp[i / 2] = two_dim_connect_by_parent(nodes[i], NULL);
            else
                nodes_temp[i / 2] = two_dim_connect_by_parent(nodes[i], nodes[i + 1]);
        }
        nodes = nodes_temp;
        rec_build_two_dim_tree(nodes, (len + 1) / 2, nodes_temp);
    }
    else ultimate_interval_tree = nodes[0];
}

void build_row_representing_interval_tree(int row_nr) {
    interval_node* all_leaves[field_width * 2 + 1] = {NULL};
    interval_node* nodes_temp[field_width * 2 + 1] = {NULL};
    for (int i = 0; i < field_width; i++) {
        vtype value;
        cin >> value;
        all_leaves[i] = new_interval_leaf(i, i, value, value);
    }
    rec_build_interval_tree(all_leaves, field_width, nodes_temp, row_nr);
}

void build_two_dimensional_tree() {
    two_dim_interval_node* all_leaves[field_height * 2 + 1] = {NULL};
    two_dim_interval_node* nodes_temp[field_height * 2 + 1] = {NULL};
    for (int i = 0; i < field_height; i++) {
        all_leaves[i] = pack_tree_into_two_dim_node(interval_rows[i], i);
    }
    rec_build_two_dim_tree(all_leaves, field_height, nodes_temp);
}

void build_all() {
    cin >> field_height >> field_width >> nr_of_queries;
    for (int i = 0; i < field_height; i++) {
        build_row_representing_interval_tree(i);
    }
    build_two_dimensional_tree();
}

} /* namespace preprocessing */

/* Methods extracting answers for specific queries from a built structure. */
/* Complexity of extracting = O(log(n) * log(m)) */
namespace solution {

values_t merge_pairs(values_t pair1, values_t pair2) {
    if (pair1.first == NO_VALUE && pair2.first == NO_VALUE)
        return pair1;
    else if (pair1.first == NO_VALUE)
        return pair2;
    else if (pair2.first == NO_VALUE)
        return pair1;
    else return make_pair(min(pair1.first, pair2.first), max(pair1.second, pair2.second));
}

values_t get_interval_values(int qleft, int qright, interval_node* node) {
    if (node == NULL || qleft > node->right_end || qright < node->left_end) {
        return make_pair(NO_VALUE, NO_VALUE);
    }
    else if (qleft <= node->left_end && node->right_end <= qright) {
        return make_pair(node->minimum, node->maximum);
    }
    else {
        return merge_pairs(get_interval_values(qleft, qright, node->left), 
                           get_interval_values(qleft, qright, node->right));
    }
}

values_t get_final_values(int qminrow, int qmaxrow, int qleft, 
                          int qright, two_dim_interval_node* node) {
    if (node == NULL || qminrow > node->max_row || qmaxrow < node->min_row) {
        return make_pair(NO_VALUE, NO_VALUE);
    }
    else if (qminrow <= node->min_row && node->max_row <= qmaxrow) {
        return get_interval_values(qleft, qright, node->merged_tree);
    }
    else {
        return merge_pairs(get_final_values(qminrow, qmaxrow, qleft, qright, node->left), 
                           get_final_values(qminrow, qmaxrow, qleft, qright, node->right));
    }
}

vtype count_fairness(values_t p) {
    return max(p.first, p.second) - min(p.first, p.second);
}

void solve_queries() {
    for (int i = 0; i < nr_of_queries; i++) {
        int qleft, qright, qrowmax, qrowmin;
        cin >> qrowmin >> qleft >> qrowmax >> qright;
        values_t final_pair = get_final_values(qrowmin, qrowmax, qleft, qright, ultimate_interval_tree);
        cout << count_fairness(final_pair) << endl;
    }
}

} /* namespace solution */

int main() {
    ios_base::sync_with_stdio(false);
    preprocessing::build_all();
    solution::solve_queries();
    return 0;
}