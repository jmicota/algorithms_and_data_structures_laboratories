#include <iostream>
#include <set>
#include <iterator>

using namespace std;

void analyze_one_input_line(int nr_of_players, int which_match, int nr_of_team[][51]) {

    int player;
    for (int i = 1; i <= nr_of_players; i++) {
        
        cin >> player;
        if (i > nr_of_players/2) {
            nr_of_team[player][which_match] = 1;
        }
    }
}

void analyze_input(int nr_of_players, int nr_of_matches, int nr_of_team[][51]) {

    for (int i = 1; i <= nr_of_matches; i++) {
        analyze_one_input_line(nr_of_players, i, nr_of_team);
    }
}

long long int generate_etiquette(int which_player, int nr_of_matches, int nr_of_team[][51]) {

    long long int to_add = 1;
    long long int etiquette = 0;

    for (int i = 1; i <= nr_of_matches; i++) {
        etiquette += nr_of_team[which_player][i] * to_add;
        to_add *= 2;
    }

    return etiquette;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n, m;
    cin >> n >> m;
    int tab[n + 1][51] = {0};
    set <long long int> my_set;

    analyze_input(n, m, tab);
    for (int i = 1; i <= n; i++) {
        long long int etiquette = generate_etiquette(i, m, tab);
        my_set.insert(etiquette);
    }

    if (my_set.size() == n) {
        cout << "TAK";
    }
    else cout << "NIE";

    return 0;
}