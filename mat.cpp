#include <iostream>
#include <string>
using namespace std;


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    string s;
    cin >> s;
    char symbol = '*';
    bool more_than_one_symbol = false;
    
    int p = 0, k = 0, min_diff = 1000005;


    while (s[p] == '*') {
        p++;
    }
    while (p < s.length()) {

        if (!more_than_one_symbol) {
            if (symbol == '*') {
                symbol = s[p];
            }
            else {
                more_than_one_symbol = (s[p] != symbol);
            }
        }

        k++;
        while (k < s.length() && s[p] == s[k] || s[k] == '*') {
            k++;
            if (s[k] == s[p]) {
                p = k;
            }
        }

        if (k < s.length()) {
            min_diff = min(min_diff, k - p);
        }
        p = k;
    }

    if (!more_than_one_symbol) {
        cout << 1;
    }
    else {
        cout << s.length() - min_diff + 1;
    }

    return 0;
}