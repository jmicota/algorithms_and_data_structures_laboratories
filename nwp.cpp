#include <bits/stdc++.h>

// JUSTYNA MICOTA 418427

using namespace std;
using vtype = long long int;
const int MAX = 1005;
const vtype NONE = -1;

int m, n;
vtype c;
vtype A[MAX], B[MAX];
vtype tab[20][MAX][MAX];

vtype lower_bound(vtype max) {
    return max - c;
}

vtype min_of(vtype a, vtype b) {
    if (a == NONE && b == NONE) return NONE;
    else if (a == NONE || b == NONE) return max(a, b);
    else if (a <= b) return a;
    return b;
}

vtype min_of_three(vtype a, vtype b, vtype c) {
    return min_of(min_of(a, b), c);
}

void get_input() {
    cin >> n >> m >> c;
    for (int i = 1; i <= n; i++)
        cin >> A[i];
    
    for (int j = 1; j <= m; j++)
        cin >> B[j];
}

void init_all() {
    for (int level = 0; level < 20; level++) {
        for (int i = 0; i <= n + 1; i++) {
            for (int j = 0; j <= m + 1; j++)
                tab[level][i][j] = NONE;
        }
    }
}

vtype choose_optimal_neighbour_cell(int i, int j, int level) {
    vtype up = tab[level][i-1][j];
    vtype left = tab[level][i][j-1];
    vtype myself = tab[level][i][j];
    return min_of_three(up, left, myself);
}

void init_one_cell_first_level(int i, int j) {
    if (A[i] == B[j]) { // przedłużamy ciąg długości 0
        vtype extender = A[i];
        tab[0][i][j] = extender;
    }
    tab[0][i][j] = choose_optimal_neighbour_cell(i, j, 0);
}

void init_first_level() {
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++)
            init_one_cell_first_level(i, j);
    }
}

void update_one_cell(int i, int j, int level) {
    if (A[i] == B[j]) { // chcemy przedłużyć
        vtype extender = A[i];
        vtype extending = tab[level-1][i-1][j-1];
        
        if (extending != NONE && extender >= lower_bound(extending)) // możemy przedłużyć
            tab[level][i][j] = max(extending, extender);
    }
    tab[level][i][j] = choose_optimal_neighbour_cell(i, j, level);
}

void update_level(int level) {
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++)
            update_one_cell(i, j, level);
    }
}

vtype get_final_from_level(int i) {
    return tab[i][n][m];
}

vtype compute() {
    init_first_level();
    vtype final = get_final_from_level(0);
    int i = 1;
    while (i < 20 && final != NONE) {
        update_level(i);
        final = get_final_from_level(i);
        i++;
    }
    if (final == NONE)
        return i - 1;
    return i;
}

int main() {
    ios_base::sync_with_stdio(false);
    get_input();
    init_all();
    cout << compute();
}