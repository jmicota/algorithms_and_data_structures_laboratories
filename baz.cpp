#include <iostream>
using namespace std;

bool odd(long long a) {
    return a % 2 == 1;
}

bool different_oddity_to(long long b, long long a) {
    return (odd(a) && !odd(b)) || (odd(b) && !odd(a));
}

long long smaller(long long diff1, long long diff2) {
    if (diff1 >= 0 && diff2 >= 0) {
        return min(diff1, diff2);
    }
    else if (diff1 < 0 && diff2 < 0) {
        return -1;
    }
    else if (diff1 <= diff2) {
        return diff1;
    }
    else return diff2;
}

// 0 -> even on left
// 1 -> odd on left
// 2 -> even on right
// 3 -> odd on right
// 4 -> postfix sum
void initiate(long long* array, long long neighbours[][5], int len) {

    int last_odd = -1, last_even = -1;

    for (int i = 0; i < len; i++) { // nieparzyste
        if (odd(array[i])) {
            neighbours[i][1] = i;
            neighbours[i][3] = i;
            last_odd = i;
            neighbours[i][0] = last_even;
        }
        else if (!odd(array[i])) { // parzyste
            neighbours[i][0] = i;
            neighbours[i][2] = i;
            last_even = i;
            neighbours[i][1] = last_odd;
        }
    }

    last_odd = -1;
    last_even = -1;
    long long sum = 0;

    for (int i = len - 1; i >= 0; i--) {

        sum += array[i];
        neighbours[i][4] = sum;

        if (odd(array[i])) {
            neighbours[i][2] = last_even;
            last_odd = i;
        }
        else {
            neighbours[i][3] = last_odd;
            last_even = i;
        }
    }
}

long long solution(int which_day, long long to_buy[], long long prices[], int nr_of_products,
                   long long neighbours[][5]) {

    if (to_buy[which_day] > nr_of_products)
        return -1;

    long long sum = neighbours[nr_of_products - to_buy[which_day]][4];

    if (odd(sum)) {
        return sum;
    }
    else {
        int pos = nr_of_products - to_buy[which_day]; // n - k
        if (nr_of_products == to_buy[which_day]) {
            return -1;
        }

        int closest_left_odd = neighbours[pos - 1][1];
        int closest_left_even = neighbours[pos - 1][0];
        int closest_right_odd = neighbours[pos][3];
        int closest_right_even = neighbours[pos][2];
        long long diff1 = 0, diff2 = 0;
        
        if (odd(prices[pos])) {
            if (closest_left_even != -1) {
                diff1 = prices[pos] - prices[closest_left_even];
            }
            else diff1 = -1;

            if (closest_right_even != -1 && closest_left_odd != -1) {
                diff2 = prices[closest_right_even] - prices[closest_left_odd];
            }
            else diff2 = -1;
        }
        else { // parzysta
            if (closest_left_odd != -1) {
                diff1 = prices[pos] - prices[closest_left_odd];
            }
            else diff1 = -1;

            if (closest_right_odd != -1 && closest_left_even != -1) {
                diff2 = prices[closest_right_odd] - prices[closest_left_even];
            }
            else diff2 = -1;
        }

        if (diff1 >= 0 && diff2 >= 0) {
            return sum - min(diff1, diff2);
        }
        else if (diff1 < 0) {
            if (diff2 >= 0) {
                return sum - diff2;
            }
            else return -1;
        }
        else return sum - diff1;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n, days;
    
    cin >> n; // n is > 0
    long long prices[n];
    for (int i = 0; i < n; i++) {
        cin >> prices[i];
    } 

    cin >> days; // days is > 0
    long long to_buy[days];
    for (int i = 0; i < days; i++) {
        cin >> to_buy[i];
    }

    long long neighbour_idx[n][5];
    initiate(prices, neighbour_idx, n);

    for (int i = 0; i < days; i++) {
        cout << solution(i, to_buy, prices, n, neighbour_idx) << '\n';
    }
    
    return 0;
}