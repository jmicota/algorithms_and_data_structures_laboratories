#include <bits/stdc++.h>


using namespace std;

int n, m, p1, p2, k1, k2;
const long long P = 2000000011;
const int baza_hasha = 29;

char slowo[300005];
long long bazy[300005];
long long hasz_sufiksu[300005];

void licz_hasze_sufiksow() {
    long long B = 1;

    for (int i = n; i > 0; i--) {
        hasz_sufiksu[i] = (hasz_sufiksu[i+1] + (slowo[i] - 'a' + 1) * B) % P;
        bazy[n-i] = B; // B[0] = 1, B[n-1] = baza^{n-1}
        B = (B * baza_hasha) % P;
    }
}

long long hasz_podslowa(int l, int r) {
    if (r == n) return hasz_sufiksu[l];
    return (hasz_sufiksu[l] - hasz_sufiksu[r+1] + P) % P;
}

pair<long long, long long> licz_hasze_podslow(int l1, int r1, int l2, int r2) {
    // cout << "licz hasze od ";
    // for (int i = l1; i <= r1; i++)
    //     cout << slowo[i];
    // cout << " ";
    // for (int i = l2; i <= r2; i++)
    //     cout << slowo[i];
    // cout << endl;
    long long hasz1 = hasz_podslowa(l1, r1);
    long long hasz2 = hasz_podslowa(l2, r2);
    if (r1 < r2) {
        int diff = r2 - r1;
        hasz2 = (hasz2 * bazy[diff]) % P;
    }
    else if (r1 > r2) {
        int diff = r1 - r2;
        hasz1 = (hasz1 * bazy[diff]) % P;
    }
    //cout<<"finalnie: "<<hasz1<<" "<<hasz2<<endl;
    return make_pair(hasz1, hasz2);
}

int binsearch_it(int pocz1, int kon1, int pocz2, int kon2) {
    int k = kon1 - pocz1;
    int p = 0;
    long long hasz1, hasz2;
    if (slowo[pocz1] != slowo[pocz2]) return 0;
    while (p < k) {
        int mid = (p + k + 1)/2;
        // cout << "while(): "<<p<<" "<<mid<<" "<<k<<endl;
        pair<long long, long long> hasze = licz_hasze_podslow(pocz1, pocz1 + mid, pocz2, pocz2 + mid);
        hasz1 = hasze.first;
        hasz2 = hasze.second;
        if (hasz1 == hasz2) {
            p = mid;
        }
        else {
            // cout << "not equal => newk = "<<mid - 1<<endl;
            k = mid - 1;
        }
    }
    // cout<<"miejsce roznicy = "<<k+1<<endl;
    return k + 1; // miejsce roznicy
}

int compare_equal(int newk1, int newk2) {
    //cout << endl << "in compare_equal(): " << p1 <<" "<<newk1<<" "<<p2<<" "<<newk2<<endl;
    pair<long long, long long> hasze = licz_hasze_podslow(p1, newk1, p2, newk2);
    long long hasz1 = hasze.first;
    long long hasz2 = hasze.second;
    //cout<<"They are equal: "<<hasz1 << " "<<hasz2<<endl;
    int ret;
    if (hasz1 == hasz2) {
        // cout << "equal hashes.\n";
        return 0;
    }
    else {
        //cout << "different hashes.\n";
        ret = binsearch_it(p1, newk1, p2, newk2);
        char c1 = slowo[p1 + ret];
        char c2 = slowo[p2 + ret];
        if (c1 > c2) return -1;
        else return 1;
    }
}

char compare() {
    int l1 = k1 - p1 + 1;
    int l2 = k2 - p2 + 1;
    //cout << "lengths: "<<l1 << " "<<l2<<endl;
    int ret;
    if (l1 != l2) {
        //cout << "diff lengths..";
        if (l1 > l2) {
            ret = compare_equal(p1 + l2 - 1, k2);
        }
        else if (l2 > l1) {
            ret = compare_equal(k1, p2 + l1 - 1);
        }
        
        if (ret == 0) {
            if (l1 > l2) {
                return '>';
            }
            else if (l2 > l1) {
                return '<';
            }
        }
        else if (ret == -1) {
            return '>';
        }
        else if (ret == 1) {
            return '<';
        }
    }
    else {
        //cout << "same lengths..";
        ret = compare_equal(k1, k2);
        if (ret == 0) return '=';
        else if (ret == 1) return '<';
        else return '>';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        cin >> slowo[i];
    }

    licz_hasze_sufiksow();

    

    for (int i = 0; i < m; i++) {
        cin >> p1 >> k1 >> p2 >> k2;
        cout << compare() << endl;
    }

}