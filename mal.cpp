#include <bits/stdc++.h>

#define BLACK 0
#define WHITE 1
#define NO_COLOR -1
#define ROOT 1
#define MAX 1000002

using namespace std;

long long suma[MAX * 4];
long long tree[MAX * 4];
long long n;

void inicjalizuj() {
    memset(tree, -1, sizeof(tree));
    tree[1] = BLACK;
}

long long suma_biala() {
    return suma[ROOT];
}

void paint_rek(long long l, long long r, int color, long long v, 
               long long vl, long long vr) {
    if (l > vr || r < vl || v < 1 || v > 4*n || l > r) {
        return;
    }
    
    if (l <= vl && r >= vr) {
        tree[v] = color;
        suma[v] = (color == WHITE ? vr - vl + 1 : 0);
    }
    else {
        long long mid = (vl + vr) / 2;
        if (tree[v] == WHITE || tree[v] == BLACK) {
            tree[2*v] = tree[v];
            tree[2*v+1] = tree[v];
            suma[2*v] = (tree[v] == WHITE ? mid - vl + 1 : 0);
            suma[2*v+1] = (tree[v] == WHITE ? vr - mid : 0);
        }

        paint_rek(l, r, color, 2*v, vl, mid);
        paint_rek(l, r, color, 2*v+1, mid + 1, vr);

        tree[v] = (tree[2*v] == tree[2*v+1] ? tree[2*v] : NO_COLOR);
        suma[v] = suma[2*v] + suma[2*v+1]; 
    }
}

void paint(long long l, long long r, int color) {
    paint_rek(l, r, color, ROOT, 1, n);
}

int main() {
    long long m, l, r;
    char c;
    cin >> n >> m;
    inicjalizuj();
    for (long long i = 0; i < m; i++) {
        cin >> l >> r >> c;
        if (c == 'C') {
            paint(l, r, BLACK);
        }
        else if (c == 'B') {
            paint(l, r, WHITE);
        }
        cout << suma_biala() << '\n';
    }
}