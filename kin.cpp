#include <iostream>
#include <assert.h>

#define MOD 1000000000
#define ROOT_IDX 1

long long DP[11][20001] = {0};

int find_power_of_two(int n) {
    long long res = 1;
    while (res < n) {
        res *= 2;
    }
    return res;
}

int idx_in_tree(int x, int tree_size) {
    return tree_size + x - 1;
}

void reset_tree(int tree_size, long long tree[]) {
    for (int i = 0; i <= 2 * tree_size; i++) {
        tree[i] = 0;
    }
}

int left_leaf(int node, int tree_size) {
    while (node < tree_size) {
        node *= 2;
    }
    return node;
}

int right_leaf(int node, int tree_size) {
    while (node < tree_size) {
        node = node * 2 + 1;
    }
    return node;
}

void add_to_tree(int x, long long value, long long tree[], int tree_size) {
    int idx = idx_in_tree(x, tree_size);
    while (idx > 0) {
        tree[idx] = (tree[idx] + value) % MOD;
        idx /= 2;
    }
}

long long count_sum(int L, int R, int main_node, long long tree[], int tree_size) {

    int left_end = left_leaf(main_node, tree_size);
    int right_end = right_leaf(main_node, tree_size);

    if (left_end == L && right_end == R) {
        return tree[main_node];
    }
    else if (L == R) {
        return tree[L];
    }
    else {
        int mid = (left_end + right_end) / 2; // "lewy" mid
        
        if (R <= mid) {
            return count_sum(L, R, main_node * 2, tree, tree_size);
        }
        if (L > mid) {
            return count_sum(L, R, main_node * 2 + 1, tree, tree_size);
        }
        else { // rozbicie na dwa przedziały
            return count_sum(L, mid, main_node * 2, tree, tree_size) + 
                   count_sum(mid + 1, R, main_node * 2 + 1, tree, tree_size);
        }
    }
}

void print_DP(int n, int k) {
    for (int i = 0; i <= n; i++) {
        for (int j = 0; j <= k; j++) {
            std::cout<<DP[i][j];
        }
        std::cout<<std::endl;
    }
}


void print_tree(int tree_size, long long tree[]) {
    for (int i = 0; i < 2 * tree_size + 1; i++) {
        std::cout<<tree[i]<<" ";
    }
    std::cout<<"\n";
}

long long sum_right_side_of_tree(int x, int n, long long tree[], int tree_size) {
    if (x > n) { 
        return 0;
    }
    else return count_sum(idx_in_tree(x, tree_size), idx_in_tree(n, tree_size), ROOT_IDX, tree, tree_size);
}

long long update_row(int row_nr, int n, int tab[], long long tree[], int tree_size) {

    reset_tree(tree_size, tree);
    long long row_sum = 0;

    for (int i = 0; i < n; i++) { // idziemy po całym ciągu
        int x = tab[i]; // dla każdej kolejnej wartości
        long long x_sum = sum_right_side_of_tree(x + 1, n, tree, tree_size); // liczymy sumę drzewa od x+1 do końca
        
        DP[row_nr][x] = x_sum; // zapisz sume w DP
        add_to_tree(x, DP[row_nr - 1][x], tree, tree_size); // dodaj odp wartość do drzewa, bo ta wartość bedzie teraz z lewej od nastepnej
        
        row_sum = (row_sum + x_sum) % MOD; // aktualizuj row_sum
    }
    return row_sum;
}

long long fill_DP_and_ret_sum_of_k(int n, int k, int tab[], long long tree[], int tree_size) {

    for (int i = 1; i <= n; i++)
        DP[1][i] = 1;

    long long sum = 0;
    for (int row_nr = 2; row_nr <= k; row_nr++) {
        sum = update_row(row_nr, n, tab, tree, tree_size);
    }
    return sum;
}

int main() {
    int n, k;
    std::cin >> n >> k;
    int tree_size = find_power_of_two(n);
    int tab[n];
    long long tree[2 * tree_size + 1] = {0};
    
    for (int i = 0; i < n; i++)
        std::cin >> tab[i];
 
    std::cout<<fill_DP_and_ret_sum_of_k(n, k, tab, tree, tree_size);
}