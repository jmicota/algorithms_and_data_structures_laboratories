#include <bits/stdc++.h>

using namespace std;
using vtype = long long;
using pairt = pair<vtype, vtype>;
const vtype MAX_D = 1000000005;
const vtype MAX_N = 200005;

int ile_wysp;
vtype x, y;
pairt wspolrzedne[MAX_N];
pairt po_xach[MAX_N];
pairt po_yach[MAX_N];
vtype poz_x[MAX_N];
vtype poz_y[MAX_N];
vtype dist[MAX_N] = {MAX_D};
priority_queue<pairt> qmin;

vtype calc_dist(pairt p1, pairt p2) {
    return min(abs(p1.first - p2.first), abs(p1.second - p2.second));
}

int main() {
    ios_base::sync_with_stdio(false);
    cin >> ile_wysp;
    for (int i = 0; i < ile_wysp; i++) {
        cin >> x >> y;
        wspolrzedne[i] = make_pair(x, y);
        pairt wx = make_pair(x, i);
        pairt wy = make_pair(y, i);
        po_xach[i] = wx;
        po_yach[i] = wy;
        dist[i] = MAX_D;
    }
    sort(po_xach, po_xach + ile_wysp);
    sort(po_yach, po_yach + ile_wysp);

    // wpisz ich pozycje w posortowanych
    for (int i = 0; i < ile_wysp; i++) {
        vtype nrwx = po_xach[i].second;
        poz_x[nrwx] = i;
        vtype nrwy = po_yach[i].second;
        poz_y[nrwy] = i;
    }
  
    vtype s = 0;
    dist[s] = 0;
    qmin.push(make_pair(0, s));
    while (!qmin.empty()) {
        pairt v = qmin.top();
        qmin.pop();
        vtype nrv = v.second;
        vtype pozx = poz_x[nrv];
        vtype pozy = poz_y[nrv];

        if (pozx > 0) {
            // idziemy w lewy x
            vtype sasiad = po_xach[pozx - 1].second;
            vtype d = calc_dist(wspolrzedne[nrv], wspolrzedne[sasiad]);
            if (d + dist[nrv] < dist[sasiad]) {
                dist[sasiad] = d + dist[nrv];
                qmin.push(make_pair(-dist[sasiad], sasiad));
            }
        }
        if (pozy > 0) {
            // idziemy w lewy y
            vtype sasiad = po_yach[pozy - 1].second;
            vtype d = calc_dist(wspolrzedne[nrv], wspolrzedne[sasiad]);
            if (d + dist[nrv] < dist[sasiad]) {
                dist[sasiad] = d + dist[nrv];
                qmin.push(make_pair(-dist[sasiad], sasiad));
            }
        }
        if (pozx < ile_wysp - 1) {
            // idziemy w prawy x
            vtype sasiad = po_xach[pozx + 1].second;
            vtype d = calc_dist(wspolrzedne[nrv], wspolrzedne[sasiad]);
            if (d + dist[nrv] < dist[sasiad]) {
                dist[sasiad] = d + dist[nrv];
                qmin.push(make_pair(-dist[sasiad], sasiad));
            }
        }
        if (pozy < ile_wysp - 1) {
            // idziemy w prawy y
            vtype sasiad = po_yach[pozy + 1].second;
            vtype d = calc_dist(wspolrzedne[nrv], wspolrzedne[sasiad]);
            if (d + dist[nrv] < dist[sasiad]) {
                dist[sasiad] = d + dist[nrv];
                qmin.push(make_pair(-dist[sasiad], sasiad));
            }
        }
    }

    cout << dist[ile_wysp - 1];
}