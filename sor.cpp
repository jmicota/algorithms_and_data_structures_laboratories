#include <iostream>
#include <string>

using namespace std;

#define MOD 1000000000
#define MAX 1000
#define RIGHT true
#define LEFT false

int R[MAX][MAX], L[MAX][MAX];

void initiate_arrays(int length) {

    for (int i = 0; i < length; i++) {
        for (int j = 0; j < length; j++) {
            L[i][j] = -1;
            R[i][j] = -1;
        }
    }

    for(int i = 0; i < length; i++) {
        L[i][i] = 1;
        R[i][i] = 1;
    }
}

int iverson(int i, int j, int array[], int length) {

    if (array[i] < array[j]) return 1;
    return 0;
}

bool not_counted_yet(int i) {
    return i == -1;
}

int count(int i, int j, bool side, int array[], int length) {

    if (side == RIGHT) {
        if (not_counted_yet(R[i][j])) {
            if (i + 1 == j) {
                R[i][j] = iverson(i, j, array, length);
            }
            else {
                R[i][j] = (iverson(i, j, array, length) * count(i, j - 1, LEFT, array, length) + 
                           iverson(j - 1, j, array, length) * count(i, j - 1, RIGHT, array, length)) % MOD;
            } 
        }
        return R[i][j];
    }

    if (side == LEFT) {
        if (not_counted_yet(L[i][j])) {
            if (i + 1 == j) {
                L[i][j] = iverson(i, j, array, length);
            }
            else {
                L[i][j] = (iverson(i, j, array, length) * count(i + 1, j, RIGHT, array, length) + 
                           iverson(i, i + 1, array, length) * count(i + 1, j, LEFT, array, length)) % MOD;
            }
        }
        return L[i][j];
    }
}

void fill_arrays(int L[MAX][MAX], int R[MAX][MAX], int array[], int length) {

    for (int i = 0; i < length; i++) {
        
        for (int j = i; j < length; j++) {

            R[i][j] = count(i, j, RIGHT, array, length);
            L[i][j] = count(i, j, LEFT, array, length);
        }
    }
}

int do_all(int L[MAX][MAX], int R[MAX][MAX], int length) {

    int array[length];
    
    for (int i = 0; i < length; i++)
        cin >> array[i];

    if (length == 1)
        return 1;
    
    initiate_arrays(length);    
    fill_arrays(L, R, array, length);

    return (L[0][length - 1] + R[0][length - 1]) % MOD;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n;
    cin >> n;
    int solution = do_all(L, R, n);
    cout << solution;
    
    return 0;
}